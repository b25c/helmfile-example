# helmfile-example

## Overview

This example installs prometheus-pushgateway into multiple environments.  You need to run `helmfile`
for each environment.

In this simple example, each environment will set a different image tag.

## Dependencies

Install dependencies (helm and helmfile) with [asdf](https://github.com/asdf-vm/asdf):

```sh
asdf install
```

## Running

To see the yaml that's generated for the staging environment, run:

```sh
helmfile --environment staging template
```

Notice that the image tag is set to `staging`.

To apply the staging environment, you can run:

```sh
helmfile --environment staging apply
```

## Example

Generated production yaml:
```sh
❯ helmfile --environment production template
Adding repo prometheus-community https://prometheus-community.github.io/helm-charts
"prometheus-community" has been added to your repositories

Templating release=prometheus-pushgateway, chart=prometheus-community/prometheus-pushgateway
---
# Source: prometheus-pushgateway/templates/serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: prometheus-pushgateway
  labels:
    app: prometheus-pushgateway
    chart: prometheus-pushgateway-1.5.0
    heritage: Helm
    release: prometheus-pushgateway
---
# Source: prometheus-pushgateway/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: prometheus-pushgateway
  annotations:
    {}
  labels:
    app: prometheus-pushgateway
    chart: prometheus-pushgateway-1.5.0
    heritage: Helm
    release: prometheus-pushgateway
spec:
  type: ClusterIP
  ports:
    - port: 9091
      targetPort: 9091
      protocol: TCP
      name: http
  selector:
    app: prometheus-pushgateway
    release: prometheus-pushgateway
---
# Source: prometheus-pushgateway/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: prometheus-pushgateway
  labels:
    app: prometheus-pushgateway
    chart: prometheus-pushgateway-1.5.0
    heritage: Helm
    release: prometheus-pushgateway
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: prometheus-pushgateway
      release: prometheus-pushgateway
  template:
    metadata:
      labels:
        app: prometheus-pushgateway
        release: prometheus-pushgateway
      annotations:
        {}
    spec:
      serviceAccountName: prometheus-pushgateway
      containers:
        - name: pushgateway
          image: "prom/pushgateway:production"
          imagePullPolicy: IfNotPresent
          ports:
            - name: metrics
              containerPort: 9091
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /-/healthy
              port: 9091
            initialDelaySeconds: 10
            timeoutSeconds: 10
          readinessProbe:
            httpGet:
              path: /-/ready
              port: 9091
            initialDelaySeconds: 10
            timeoutSeconds: 10
          resources:
            {}
```

